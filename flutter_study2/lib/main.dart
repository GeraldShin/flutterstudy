import 'package:flutter/material.dart';

// 레이아웃 참고 사이트  https://medium.com/flutter-community/flutter-layout-cheat-sheet-5363348d037e
void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'FullterStudy2',
      theme: ThemeData(primaryColor: Colors.blue),
      home: MyPage(),
    );
  }
}

class MyPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: Colors.blue,
      body: SafeArea(
        child: Center(
            child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Column(
              // 새로축 방향으로 Layer 를 배열, 센터위젯에 자식으로 컬럼이 오면 세로축을 전부 가져가서 세로 정렬이 안된다.
              //mainAxisAlignment: MainAxisAlignment.center, // Column 안에서 새로축 정렬을 이한 옵션
              mainAxisSize: MainAxisSize
                  .min, // 컬럼 위젯이 세로축 전체를 사용하지 않고 팔요한 만큼만 사용하도록 하려면 사용
              children: [
                Container(
                  width: 100,
                  height: 100,
                  color: Colors.white,
                  child: Text('Container 1'),
                ),
                SizedBox(
                  height: 30,
                ),
                Container(
                  width: 100,
                  height: 100,
                  color: Colors.red,
                  child: Text('Container 2'),
                ),
                SizedBox(
                  height: 30,
                ),
                Container(
                  width: 100,
                  height: 100,
                  color: Colors.yellow,
                  child: Center(child: Text('Container 3')),
                )
              ],
            ),
            Row(
              children: [
                Container(
                  width: 100,
                  height: 100,
                  color: Colors.white,
                  child: Text('Container 1'),
                ),
                SizedBox(
                  width: 30,
                ),
                Container(
                  width: 100,
                  height: 100,
                  color: Colors.red,
                  child: Text('Container 2'),
                ),
                SizedBox(
                  width: 30,
                ),
                Container(
                  width: 100,
                  height: 100,
                  color: Colors.yellow,
                  child: Center(child: Text('Container 3')),
                )
              ],
            ),
          ],
        )),
      ),
    );
  }
}
