import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Appbar',
      theme: ThemeData(primarySwatch: Colors.red),
      home: MyPage(),
    );
  }
}

class MyPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            title: const Text('Appbar icon'),
            centerTitle: true,
            elevation: 0.0,
            // leading: IconButton(
            //   icon: const Icon(Icons.menu),
            //   onPressed: () {
            //     // ignore: avoid_print
            //     print('menu button is clicked');
            //   },
            // ),
            actions: <Widget>[
              IconButton(
                  onPressed: () {
                    // ignore: avoid_print
                    print('Shopping cart button is clicked');
                  },
                  icon: const Icon(Icons.shopping_cart)),
              IconButton(
                  onPressed: () {
                    // ignore: avoid_print
                    print('Search button is clicked');
                  },
                  icon: const Icon(Icons.search))
            ]),
        body: Builder(
          builder: (BuildContext ctx) {
            return Center(
              child: FlatButton(
                child: Text(
                  'Show me',
                  style: TextStyle(color: Colors.white),
                ),
                color: Colors.red,
                onPressed: () {
                  Scaffold.of(ctx)
                      .showSnackBar(SnackBar(content: Text('Hellow')));
                },
              ),
            );
          },
        ),
        drawer: Drawer(
          child: ListView(
            padding: EdgeInsets.zero,
            children: <Widget>[
              UserAccountsDrawerHeader(
                currentAccountPicture: CircleAvatar(
                  backgroundImage: AssetImage('assets/img_pikachu_01.png'),
                  backgroundColor: Colors.white,
                ),
                otherAccountsPictures: [
                  CircleAvatar(
                    backgroundColor: Colors.white,
                    backgroundImage: AssetImage('assets/img_pikachu_02.png'),
                  )
                ],
                accountName: Text('Gerald'),
                accountEmail: Text('sera7scg@gmail.com'),
                onDetailsPressed: () {
                  print('arrow is clicked');
                },
                decoration: BoxDecoration(
                    color: Colors.red[200],
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(10.0),
                        bottomRight: Radius.circular(10.0))),
              ),
              ListTile(
                  leading: Icon(Icons.home, color: Colors.grey[850]),
                  title: Text('Home'),
                  onTap: () {
                    print('Home is clicked');
                  },
                  trailing: Icon(Icons.add)),
              ListTile(
                  leading: Icon(Icons.settings, color: Colors.grey[850]),
                  title: Text('Setting'),
                  onTap: () {
                    print('setting is clicked');
                  },
                  trailing: Icon(Icons.add)),
              ListTile(
                  leading: Icon(Icons.question_answer, color: Colors.grey[850]),
                  title: Text('Q&A'),
                  onTap: () {
                    print('Q&A is clicked');
                  },
                  trailing: Icon(Icons.add))
            ],
          ),
        ));
  }
}
