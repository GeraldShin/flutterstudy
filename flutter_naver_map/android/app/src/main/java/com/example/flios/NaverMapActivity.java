// Copyright 2014 The Flutter Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package com.example.flios;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.naver.maps.geometry.LatLng;
import com.naver.maps.map.CameraAnimation;
import com.naver.maps.map.CameraUpdate;
import com.naver.maps.map.MapView;
import com.naver.maps.map.NaverMap;
import com.naver.maps.map.OnMapReadyCallback;
import com.naver.maps.map.UiSettings;
import com.naver.maps.map.overlay.Marker;
import com.naver.maps.map.overlay.Overlay;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class NaverMapActivity extends AppCompatActivity implements OnMapReadyCallback, View.OnClickListener {
  public static final String EXTRA_TITLE = "title";

  private TextView mTxvTitle;
  private ImageView mImvBackBtn;
  private MapView mLayMapView;
  private NaverMap mNaverMap;

  private ArrayList<DummyData> entryDummyData = new ArrayList<DummyData>();

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.android_full_screen_layout);

    loadDummyData();

    initLayout(savedInstanceState);
  }

  private void initLayout(Bundle savedInstanceState){
    mTxvTitle = findViewById(R.id.title_sub_text);
    mTxvTitle.setText("Android 네이버 지도");
    mImvBackBtn = findViewById(R.id.title_sub_back);
    mImvBackBtn.setOnClickListener(this);

    mLayMapView = findViewById(R.id.lay_naver_map);
    mLayMapView.onCreate(savedInstanceState);
    mLayMapView.getMapAsync(this);
  }

  private void loadDummyData(){
    entryDummyData.add(new DummyData(33.50993230401013, 126.46637308285129, "도두항"));
    entryDummyData.add(new DummyData(33.52119039517549, 126.53159657364615, "제주항"));
    entryDummyData.add(new DummyData(33.52576884212464, 126.56477925730593, "화북포구"));
    entryDummyData.add(new DummyData(33.51827522457252, 126.50056883366791, "용담포구"));
    entryDummyData.add(new DummyData(33.52748440268036, 126.57672027848749, "삼양방파제"));
  }

  private void returnToFlutterView(String strTitle) {
    Intent returnIntent = new Intent();
    returnIntent.putExtra(EXTRA_TITLE, strTitle);
    setResult(Activity.RESULT_OK, returnIntent);
    finish();
  }

  public void onBackPressed() {
    returnToFlutterView("");
  }


  @Override
  public void onMapReady(@NonNull @NotNull NaverMap naverMap) {
    naverMap.setMinZoom(6);
    naverMap.setMaxZoom(19);
    //naverMap.setSymbolScale(0);
    naverMap.setLayerGroupEnabled(NaverMap.LAYER_GROUP_BUILDING, true);

    CameraUpdate cameraUpdate = CameraUpdate.scrollAndZoomTo(new LatLng(33.505119657129434, 126.5256633637944), 10)
            .reason(3)
            .animate(CameraAnimation.Fly, 0)
            .finishCallback(() -> {

            })
            .cancelCallback(() -> {

            });

    naverMap.moveCamera(cameraUpdate);

    UiSettings uiSettings = naverMap.getUiSettings();
    uiSettings.setScaleBarEnabled(false);
    uiSettings.setZoomControlEnabled(false);

    mNaverMap = naverMap;

    showMarkerData();
  }

  private void showMarkerData(){
     for(DummyData data : entryDummyData){
       Marker marker = new Marker();
       marker.setPosition(new LatLng(data.lat, data.lng));
       marker.setCaptionText(data.title);
       Bundle bundle = new Bundle();
       bundle.putString("title", data.title);
       marker.setTag(bundle);

       marker.setOnClickListener(new Overlay.OnClickListener() {
         @Override
         public boolean onClick(@NonNull Overlay overlay) {
           Bundle bundle = (Bundle) overlay.getTag();
           String title = bundle.getString("title");
           returnToFlutterView(title);
           return false;
         }
       });
       marker.setMap(mNaverMap);
     }
  }

  @Override
  public void onClick(View view) {
    if(view == mImvBackBtn){
      finish();
    }
  }

  public class DummyData {

    public double lat;
    public double lng;
    public String title;

    public DummyData(double lat, double lng, String title){

      this.lat = lat;
      this.lng = lng;
      this.title = title;
    }
  }
}

