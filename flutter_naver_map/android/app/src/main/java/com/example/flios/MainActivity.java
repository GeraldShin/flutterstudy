package com.example.flios;
import android.content.Intent;

import androidx.annotation.NonNull;
import io.flutter.embedding.android.FlutterActivity;

import io.flutter.embedding.engine.FlutterEngine;
import io.flutter.plugin.common.MethodCall;

import io.flutter.plugin.common.MethodChannel;

public class MainActivity extends FlutterActivity {

    private static final String CHANNEL = "com.objectbeam.flios/nativeApp";
    private static final String METHOD_SWITCH_VIEW = "naverMapView";
    private static final int COUNT_REQUEST = 1;

    private MethodChannel.Result result;

    @Override
    public void configureFlutterEngine(@NonNull FlutterEngine flutterEngine) {
        new MethodChannel(flutterEngine.getDartExecutor(), CHANNEL).setMethodCallHandler(
                new MethodChannel.MethodCallHandler() {
                    @Override
                    public void onMethodCall(MethodCall methodCall, MethodChannel.Result result) {
                        MainActivity.this.result = result;
                        //int count = methodCall.arguments();
                        if (methodCall.method.equals(METHOD_SWITCH_VIEW)) {
                            onLaunchFullScreen(0);
                        } else {
                            result.notImplemented();
                        }
                    }
                }
        );
    }

    private void onLaunchFullScreen(int count) {
        Intent fullScreenIntent = new Intent(this, NaverMapActivity.class);
        startActivityForResult(fullScreenIntent, COUNT_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == COUNT_REQUEST) {
            if (resultCode == RESULT_OK) {
                String title = data.getStringExtra("title");
                result.success(title);
            } else {
                result.error("ACTIVITY_FAILURE", "Failed while launching activity", null);
            }
        }
    }
}