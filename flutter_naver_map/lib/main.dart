import 'package:flios/second_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/foundation.dart' as foundation;

void main() => runApp(MyApp());

class PositionData {
  String title;
  double lat;
  double lng;

  PositionData(String title, double lat, double lng) {
    this.title = title;
    this.lat = lat;
    this.lng = lng;
  }
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Naver Map',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter to iOS or AOS'),
      // ignore: missing_return
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;
  BuildContext mContext;

  static const platform = const MethodChannel('com.objectbeam.flios/nativeApp');

  // Android 또는 iOS Page 호출
  Future<void> _goToNative() async {
    switch (foundation.defaultTargetPlatform) {
      case foundation.TargetPlatform.iOS:
        print("OS Type: iOS");
        break;
      case foundation.TargetPlatform.android:
        print("OS Type: Android.");
        break;
    }

    try {
      final String result = await platform.invokeMethod('naverMapView');
      debugPrint('Result: $result ');
      final positionData = PositionData(result, 0.0, 0.0);

      //_showMyDialog(result);
      Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => SecondPage(positionData: positionData)),
      );
    } on PlatformException catch (e) {
      debugPrint("Error: '${e.message}'.");
    }
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    mContext = context;

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'Flutter에서 Native로\n개발된 Naver Map View 호출',
            ),
            ElevatedButton(onPressed: _goToNative, child: Text('Naver Map 호출'))
          ],
        ),
      ),
    );
  }

  Future<void> _showMyDialog(String strData) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('알림'),
          content: SingleChildScrollView(
            child: ListBody(
              children: const <Widget>[
                Text('Native Naver Map 에서 를 선택 하였습니다.'),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: const Text('확인'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}

class SecondRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Second Route"),
      ),
      body: Center(
        child: RaisedButton(
          onPressed: () {
            Navigator.pop(context);
          },
          child: Text('Go back!'),
        ),
      ),
    );
  }
}
