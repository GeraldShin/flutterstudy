import 'package:flios/main.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SecondPage extends StatelessWidget {
  // This widget is the root of your application.
  final PositionData positionData;

  SecondPage({@required this.positionData});

  @override
  Widget build(BuildContext context) {
    String title = positionData.title;

    return Scaffold(
      appBar: AppBar(
        title: Text('Flutter 페이지'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'Navite Naver Map에서 선택한 Point 데이터\n($title)',
            ),
          ],
        ),
      ),
    );
  }
}
