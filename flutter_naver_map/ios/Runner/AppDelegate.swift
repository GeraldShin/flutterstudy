import UIKit
import Flutter
import NMapsMap

@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {
    private var mainCoordinator: AppCoordinator?
    private var flutterResult:FlutterResult?
    
    override func application(
        _ application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
    ) -> Bool {
        
        NMFAuthManager.shared().clientId = "srnhmmdtuq"
        
        GeneratedPluginRegistrant.register(with: self)
        
        let flutterViewController: FlutterViewController = window?.rootViewController as! FlutterViewController
        //
        let tstChannel = FlutterMethodChannel(name: "com.objectbeam.flios/nativeApp",
                                              binaryMessenger: flutterViewController.binaryMessenger)
        tstChannel.setMethodCallHandler({
            (call: FlutterMethodCall, result: @escaping FlutterResult) -> Void in
            guard call.method == "naverMapView" else {
                result(FlutterMethodNotImplemented)
                return
            }
            
            self.flutterResult = result
            self.mainCoordinator?.start()
        })
        
        //
        GeneratedPluginRegistrant.register(with: self)
        //
        let navigationController = UINavigationController(rootViewController: flutterViewController)
        navigationController.isNavigationBarHidden = true
        window?.rootViewController = navigationController
        mainCoordinator = AppCoordinator(navigationController: navigationController)
        window?.makeKeyAndVisible()
        
        return super.application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    
    func resultNativeToFlutter(strData: String){
        if(flutterResult != nil){
            flutterResult!(strData)
        }
    }
    
}
