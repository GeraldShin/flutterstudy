
import UIKit
import NMapsMap


class NewsViewController: UIViewController {
    var coordinatorDelegate: NewsCoordinatorDelegate?
    
    @IBOutlet weak var mMapView: NMFMapView!
    
    typealias DUMMY_DATA = (lat: Double, lng: Double, title: String)
    
    var entryDummyData:[DUMMY_DATA] = [DUMMY_DATA]();
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadDummyData();
        
        initNaverMap(lat: 33.505119657129434, lng: 126.5256633637944);
    }
    
    func loadDummyData(){
        entryDummyData.append((lat:33.50993230401013, lng: 126.46637308285129, title: "도두항"))
        entryDummyData.append((lat:33.52119039517549, lng: 126.53159657364615, title: "제주항"))
        entryDummyData.append((lat:33.52576884212464, lng: 126.56477925730593, title: "화북포구"))
        entryDummyData.append((lat:33.51827522457252, lng: 126.50056883366791, title: "용담포구"))
        entryDummyData.append((lat:33.52748440268036, lng: 126.57672027848749, title: "삼양방파제"))
    }
    
    @IBAction func goToFlutter(_ sender: Any) {
        coordinatorDelegate?.navigateToFlutter()
    }
    
    func initNaverMap(lat: Double, lng: Double){
        mMapView.setLayerGroup(NMF_LAYER_GROUP_BUILDING, isEnabled: true)
        //mMapView.setLayerGroup(NMF_LAYER_GROUP_CADASTRAL, isEnabled: true)
        mMapView.minZoomLevel = 6
        mMapView.maxZoomLevel = 19
        mMapView.addCameraDelegate(delegate: self)
        mMapView.locationOverlay.hidden = false

        let cameraUpdate = NMFCameraUpdate(scrollTo: NMGLatLng(lat: lat, lng: lng), zoomTo: 10)
        cameraUpdate.reason = 3
        cameraUpdate.animation = .fly
        cameraUpdate.animationDuration = 0
        mMapView.moveCamera(cameraUpdate, completion: { (isCancelled) in
            if isCancelled {
                print("카메라 이동 취소")
            } else {
                print("카메라 이동 성공")
            }
        })
        
        showMarkerData()
    }
    
    func showMarkerData(){
        for data in entryDummyData {
            let marker = NMFMarker()
            marker.position = NMGLatLng(lat: data.lat, lng: data.lng)
            marker.mapView = mMapView
            marker.userInfo = ["title" : data.title]
            marker.captionText = data.title
            marker.touchHandler = { (overlay) in
                let userInfo:[String: String] = overlay.userInfo as! [String: String]
                
                if let title:String = userInfo["title"] {
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.resultNativeToFlutter(strData: title)
                    self.finish(animated: true)
                    return true
                }
                return false
            }
        }
    }

    func finish(animated:Bool){
        if let navigation = self.navigationController {
            if navigation.viewControllers.count == 1 {
                self.dismiss(animated: animated, completion: nil)
            } else {
                navigation.popViewController(animated: animated)
            }
        } else {
            self.dismiss(animated: animated, completion: nil)
        }
    }
    
}

extension NewsViewController: NMFMapViewCameraDelegate {
    func mapView(_ mapView: NMFMapView, cameraIsChangingByReason reason: Int) {
    }

    func mapView(_ mapView: NMFMapView, cameraDidChangeByReason reason: Int, animated: Bool) {
    }

    func mapViewCameraIdle(_ mapView: NMFMapView) {
    }
    
    func mapView(_ mapView: NMFMapView, cameraWillChangeByReason reason: Int, animated: Bool) {
    }
}
